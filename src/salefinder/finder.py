# coding=utf-8

from logbook import Logger

from salefinder.config import SEARCHES_COLLECTIONS_NAME, SEARCH_URLS, SEARCH_NEEDED_STRINGS_LISTS, \
    SEARCH_BAD_STRINGS_LIST, SEARCH_LOCATIONS, DB_SERVER_URI, POSTS, YAD2_TYPE, SEARCH_TYPE, POST_IS_ACTIVE, \
    SEARCH_NAME, SEARCH_MAIL_ADDR, DB_ID, PISHPESHUK_TYPE, SEARCH_MAX_LENGTH, SEARCH_GROUP_IDS, YAD2_DETAILS, \
    PISHPESHUK_DETAILS
from salefinder.differences_merger import DifferencesMerger
from salefinder.pishpeshuk.config_pishpeshuk import ACCESS_TOKEN
from salefinder.pishpeshuk.pishpeshuk_post_finder import PishpeshukPostFinder
from salefinder.utils import get_collection, send_mail, log_setup
from salefinder.yad2.yad2_ad_finder import Yad2AdFinder


class SaleFinder(object):
    def __init__(self):
        log_setup.push_thread()
        self._logger = Logger('SaleFinderMain')
        self._searches_collection = None

    def find_all_posts(self):
        self._logger.info('Started SaleFinder main.')
        self._searches_collection = get_collection(DB_SERVER_URI, SEARCHES_COLLECTIONS_NAME)
        for search in self._searches_collection.find(filter={POST_IS_ACTIVE: True}):
            try:
                self._handle_whole_search(search)
            except Exception as e:
                self._logger.error('Caught exception in main, in search %s:' % search[SEARCH_NAME])
                self._logger.exception(e)

    def _handle_whole_search(self, search):
        found_posts = self._find_new_posts(search)
        difference_finder = DifferencesMerger(search, found_posts)
        search[POSTS] = difference_finder.calc_updated_posts_list()
        self._searches_collection.find_one_and_replace({DB_ID: search[DB_ID]}, search)
        self._send_mails(search, difference_finder.brand_new_posts)

    @staticmethod
    def _find_new_posts(search):
        finder = None
        if search[SEARCH_TYPE] == YAD2_TYPE:
            finder = Yad2AdFinder(search[SEARCH_NEEDED_STRINGS_LISTS], search[SEARCH_BAD_STRINGS_LIST],
                                  search[SEARCH_MAX_LENGTH], search[YAD2_DETAILS][SEARCH_URLS],
                                  search[YAD2_DETAILS][SEARCH_LOCATIONS])
        elif search[SEARCH_TYPE] == PISHPESHUK_TYPE:
            finder = PishpeshukPostFinder(search[SEARCH_NEEDED_STRINGS_LISTS], search[SEARCH_BAD_STRINGS_LIST],
                                          search[SEARCH_MAX_LENGTH], search[PISHPESHUK_DETAILS][SEARCH_GROUP_IDS],
                                          ACCESS_TOKEN)
        return finder.get_relevant_posts()

    def _send_mails(self, search, brand_new_posts):
        for new_post in brand_new_posts:
            send_mail((u'נמצאה מודעה חדשה לחיפוש %s' % search[SEARCH_NAME]).encode('utf-8'),
                      search[SEARCH_MAIL_ADDR], new_post.__repr__())
            self._logger.info('Found new post for %s, id: %s' % (search[SEARCH_NAME], new_post.post_id))
