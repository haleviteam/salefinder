class Post(object):
    DB_ID = '_id'
    DB_MESSAGE = 'message'
    DB_RELEVANT_BY_ADMIN = 'relevant'
    DB_LINK = 'link'
    DB_LAST_UPDATED = 'last_updated'
    DB_STRINGS_ANY_OF = 'any_of'
    _LINK_FORMAT = "https://www.facebook.com/groups/%s/permalink/%s/"

    def __init__(self, post_id, message):
        self.post_id = post_id
        self.message = message
        self.relevant_by_admin = True

    def __repr__(self):
        return "Post:\nMessage:%s\n\n" % self.message

    def __str__(self):
        return self.__repr__()

    def __unicode__(self):
        return self.__repr__()

    def to_mongo(self):
        return {
            self.DB_ID: self.post_id,
            self.DB_MESSAGE: self.message,
            self.DB_RELEVANT_BY_ADMIN: self.relevant_by_admin,
        }

    def to_mongo_according_to_db(self, object_from_db):
        pass

    def has_strings_from_all_lists(self, needed_strings_lists):
        has_all = True
        for string_dict in needed_strings_lists:
            string_list = string_dict[self.DB_STRINGS_ANY_OF]
            has_any = False
            if not all([string == '' for string in string_list]):
                for string in string_list:
                    if string and string in self.message:
                        has_any = True
                if not has_any:
                    has_all = False
        return has_all

    def has_none_of(self, bad_strings):
        for string in bad_strings:
            if string and string in self.message:
                return False
        return True

    def is_not_longer_than(self, max_length=None):
        if not max_length:
            return True
        return len(self.message) <= max_length
