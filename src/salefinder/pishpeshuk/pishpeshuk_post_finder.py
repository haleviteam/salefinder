from json import loads
from time import mktime, strptime
from urllib2 import urlopen

from salefinder.pishpeshuk.config_pishpeshuk import PISHPESHUK_AMOUNT_IN_PAGE, PISHPESHUK_HOURS_BACK
from salefinder.pishpeshuk.pishpeshuk_post import PishpeshukPost


class PishpeshukPostFinder(object):
    _URL = "https://graph.facebook.com/v2.5/%s/feed?access_token=%s&limit=%d"
    _DATA = "data"
    _ID = "id"
    _MESSAGE = "message"
    _UPDATED_TIME = "updated_time"
    _TIME_FORMAT = "%Y-%m-%dT%H:%M:%S+0000"

    def __init__(self, needed_strings_lists, bad_strings_list, max_message_len, group_ids, access_token,
                 amount_in_page=PISHPESHUK_AMOUNT_IN_PAGE, hours_back=PISHPESHUK_HOURS_BACK):
        self._urls = [self._URL % (group_id, access_token, amount_in_page) for group_id in group_ids if group_id]
        self.needed_strings_lists = needed_strings_lists
        self.bad_strings_list = bad_strings_list
        self.max_message_len = max_message_len
        self.hours_back = hours_back

    @classmethod
    def _timestamp_from_time_str(cls, time_str):
        return mktime(strptime(time_str, cls._TIME_FORMAT))

    def _get_list_of_posts(self):
        posts = []
        for url in self._urls:
            original_str = urlopen(url).read()
            jsons = loads(original_str)[self._DATA]
            posts += [
                PishpeshukPost(j[self._ID], j[self._MESSAGE], self._timestamp_from_time_str(j[self._UPDATED_TIME])) for
                j in jsons if self._ID in j.keys() and self._MESSAGE in j.keys() and self._UPDATED_TIME in j.keys()]
        return posts

    def get_relevant_posts(self):
        return [post for post in self._get_list_of_posts() if
                post.is_relevant_by_time(self.hours_back) and post.has_strings_from_all_lists(
                    self.needed_strings_lists) and post.has_none_of(self.bad_strings_list) and post.is_not_longer_than(
                    self.max_message_len)]
