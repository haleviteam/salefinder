# coding=utf-8

from datetime import datetime, timedelta

from salefinder.post import Post


class PishpeshukPost(Post):
    def __init__(self, post_id, message, timestamp):
        super(PishpeshukPost, self).__init__(post_id, message)
        self._timestamp = timestamp

    def __repr__(self):
        return (u"מודעה מפשפשוק:\n\nתאריך:\n%s\nלינק:\n%s\nתוכן המודעה:\n\n%s\n\n" % (
            self.last_updated, self.get_link(), self.message)).encode('utf-8')

    @property
    def last_updated(self):
        return datetime.fromtimestamp(self._timestamp)

    def get_link(self):
        group_id, post_id = self.post_id.split("_")
        return self._LINK_FORMAT % (group_id, post_id)

    def is_relevant_by_time(self, hours_back):
        return datetime.now() - self.last_updated <= timedelta(hours=hours_back)

    def to_mongo(self):
        mongo_dict = super(PishpeshukPost, self).to_mongo()
        mongo_dict[self.DB_LINK] = self.get_link()
        mongo_dict[self.DB_LAST_UPDATED] = self.last_updated
        return mongo_dict

    def to_mongo_according_to_db(self, object_from_db):
        self.relevant_by_admin = object_from_db[self.DB_RELEVANT_BY_ADMIN]
        if self.message != object_from_db[self.DB_MESSAGE]:
            self.relevant_by_admin = True
        return self.to_mongo()
