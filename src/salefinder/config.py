import os

DB_SERVER_URI = 'mongodb://root:root@ds011872.mlab.com:11872/sale'
SEARCHES_COLLECTIONS_NAME = 'searches'

YAD2_TYPE = 'yad2'
PISHPESHUK_TYPE = 'pishpeshuk'

DB_ID = '_id'
POST_IS_ACTIVE = 'active'
SEARCH_NAME = 'name'
SEARCH_TYPE = 'type'
SEARCH_MAIL_ADDR = 'mail'
SEARCH_NEEDED_STRINGS_LISTS = 'needed_strings_lists'
SEARCH_BAD_STRINGS_LIST = 'bad_strings_list'
SEARCH_MAX_LENGTH = 'max_length'
POSTS = 'posts'

YAD2_DETAILS = 'yad2_details'
SEARCH_URLS = 'urls'
SEARCH_LOCATIONS = 'locations'

PISHPESHUK_DETAILS = 'pishpeshuk_details'
SEARCH_GROUP_IDS = 'group_ids'

MAIL_FROM_ADDR = "halevitemp@gmail.com"
MAIL_SENDER_NAME = "Halevi's Finder"
GMAIL_USERNAME = MAIL_FROM_ADDR
GMAIL_PASSWORD = "halevi123"
GMAIL_SMTP_ADDR = "smtp.gmail.com:587"

if os.name == 'nt':
    LOG_PATH = r'C:\Users\Guy\Documents\Logs\salefinder\searches'
else:
    LOG_PATH = '/var/log/salefinder/searches'
