from logbook import Logger

from salefinder.config import POSTS
from salefinder.post import Post


class DifferencesMerger(object):
    def __init__(self, search, found_posts):
        self._logger = Logger('DifferencesMerger')
        self._search = search
        self._found_posts = found_posts
        self.brand_new_posts = []

    def calc_updated_posts_list(self):
        db_posts = self._search[POSTS]
        for new_post in self._found_posts:
            post_from_db = [db_post for db_post in db_posts if db_post[Post.DB_ID] == new_post.post_id]
            if post_from_db:
                db_post = post_from_db[0]
                db_posts.pop(db_posts.index(db_post))
                self._logger.debug('Found again an old one:%s' % new_post.post_id)
                db_posts.append(new_post.to_mongo_according_to_db(db_post))
            else:
                self._logger.info('Found a brand new post %s' % new_post.post_id)
                db_posts.append(new_post.to_mongo())
                self.brand_new_posts.append(new_post)
        return db_posts
