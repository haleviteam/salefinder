# coding=utf-8
from datetime import datetime
from hashlib import md5
from time import mktime

from salefinder.yad2.yad2_post import Yad2Post


class AdElement(object):
    _LINK_TEMPLATE = 'http://www.yad2.co.il/Yad2/Yad2_info.php?SalesID=%s'

    def __init__(self, webdriver_in_iframe, ad_id, date):
        self._webdriver = webdriver_in_iframe
        self.is_premium = self._find_if_ad_is_premium()
        self.link = self._get_link_from_ad_id(ad_id)
        self.date = date

    def make_post_object(self):
        ad_id = self._get_id()
        description = self._get_description()
        price = self._get_price()
        locations = self._get_locations()
        post = Yad2Post(ad_id, description, price, locations, self.link, self._datetime)
        return post

    @property
    def _datetime(self):
        if not self.date:
            return ''
        return datetime.fromtimestamp(mktime(datetime.strptime(self.date, "%d.%m.%y").timetuple()))

    def mark_as_favorite(self):
        non_favorite_elements = self._webdriver.find_elements_by_css_selector('a.ad_favorite_unmarked')
        if non_favorite_elements:
            non_favorite_element = non_favorite_elements[0]
            non_favorite_element.click()

    def _find_if_ad_is_premium(self):
        price_table = self._webdriver.find_element_by_css_selector("table.price")
        inner_tables = price_table.find_elements_by_css_selector("table")
        return len(inner_tables) == 0

    def _get_id(self):
        text_obj_div = self._webdriver.find_element_by_xpath("//img[@alt='צור קשר']/../..")
        text = text_obj_div.text
        return str(int(md5(text.encode('utf-8')).hexdigest()[:8], 16))

    def _get_description(self):
        text_obj_div = self._webdriver.find_element_by_xpath("//img[@alt='פרטים נוספים']/../..")
        text_obj_table = text_obj_div.find_element_by_css_selector("table")
        try:
            text_obj = text_obj_table.find_element_by_css_selector("div")
        except:
            return ''
        return text_obj.text

    def _get_price(self):
        price_table = self._webdriver.find_element_by_css_selector("table.price")
        inner_tables = price_table.find_elements_by_css_selector("table")
        if not self.is_premium:
            price_table = inner_tables[0]
            price_td = price_table.find_element_by_css_selector("td")
        else:
            price_td = price_table.find_elements_by_css_selector("td")[1]
        price_text = price_td.text
        price_int = self._get_price_from_text(price_text)
        return price_int

    @classmethod
    def _get_link_from_ad_id(cls, full_ad_id):
        needed_id = full_ad_id.split('_')[-1]
        return cls._LINK_TEMPLATE % needed_id

    @staticmethod
    def _get_price_from_text(price_text):
        return int(filter(lambda x: x in (str(i) for i in range(10)), price_text) or -1)

    def _get_locations(self):
        if self.is_premium:
            pass
            details_div = self._webdriver.find_element_by_xpath("//img[@alt='פרטים']/../..")
            details_table = details_div.find_element_by_css_selector("table.innerDetailsDataGrid")
            details_trs = details_table.find_elements_by_css_selector("tr")
            locations_tr = details_trs[len(details_trs) - 1]
            location_elements = locations_tr.find_elements_by_css_selector('b')
        else:
            contact_div = self._webdriver.find_element_by_xpath("//img[@alt='צור קשר']/../..")
            contact_table = contact_div.find_element_by_css_selector("table.innerDetailsDataGrid")
            location_elements = contact_table.find_elements_by_css_selector('b')
        locations = []
        for location in location_elements:
            locations.append(location.text)
        return locations
