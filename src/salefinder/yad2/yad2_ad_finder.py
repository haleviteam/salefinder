import os
from subprocess import PIPE
from subprocess import Popen
from time import sleep

from logbook import Logger
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from salefinder.yad2.ad_element import AdElement
from salefinder.yad2.config_yad2 import FIREFOX_PROFILE_PATH


class Yad2AdFinder(object):
    def __init__(self, needed_strings_lists, bad_strings_list, max_message_len, main_urls, locations_dict,
                 amount_of_pages=1):
        self._logger = Logger('Yad2Finder')
        self.needed_strings_lists = needed_strings_lists
        self.bad_strings_list = bad_strings_list
        self.max_message_len = max_message_len
        self.main_urls = [url for url in main_urls if url]
        self.locations_dict = locations_dict
        self.amount_of_pages = amount_of_pages
        self.faults = 0

    def get_relevant_posts(self):
        all_ads = []
        for url in self.main_urls:
            for page_index in xrange(self.amount_of_pages):
                page_url = '%s&Page=%d' % (url, page_index + 1)
                all_ads += self._find_all_ads(page_url)
            self._logger.debug('Amount of all relevant ads: %d' % (len(all_ads)))
        return all_ads

    @staticmethod
    def _setup_on_linux_if_needed():
        if os.name == 'posix':
            p = Popen(['pgrep', 'Xvfb'], stdout=PIPE, stderr=PIPE)
            p.wait()
            if not p.stdout.read():
                os.system('Xvfb :1 -screen 0 1024x768x24 &')
            os.environ['DISPLAY'] = ':1'

    def _find_all_ads(self, url):
        ads = list()
        firefox_profile = webdriver.FirefoxProfile()
        self._setup_on_linux_if_needed()
        firefox_driver = webdriver.Firefox(firefox_profile)
        try:
            firefox_driver.get(url)
            self._logger.info('in driver')
            sleep(10)
            ad_html_elements = firefox_driver.find_elements_by_css_selector("tr[id*='tr_Ad']")
            ids = []
            dates = []
            for ad_html_element in ad_html_elements:
                element = ad_html_element.find_elements_by_css_selector("td[onclick*='show_ad']")[0]
                ids.append(ad_html_element.get_attribute('id'))
                try:
                    dates.append(ad_html_element.find_elements_by_tag_name('span')[-1].text)
                except IndexError:
                    dates.append('')
                element.click()
                sleep(2)
            iframes = firefox_driver.find_elements_by_css_selector("iframe[id*='ad_iframe']")
            for i in xrange(len(iframes)):
                iframe = iframes[i]
                ad_id = ids[i]
                ad_date = dates[i]
                firefox_driver.switch_to_frame(iframe)
                try:
                    ad_element = AdElement(firefox_driver, ad_id, ad_date)
                    post_object = ad_element.make_post_object()
                    if post_object.has_strings_from_all_lists(self.needed_strings_lists) and post_object.has_none_of(
                            self.bad_strings_list) and post_object.is_in_good_location(
                            self.locations_dict) and post_object.is_not_longer_than(self.max_message_len):
                        ads.append(post_object)
                        ad_element.mark_as_favorite()
                except NoSuchElementException:
                    self._logger.error('Caught (CAPTCHA?) Exception:')
                    self._logger.exception()
                except Exception:
                    self._logger.exception()
                finally:
                    firefox_driver.switch_to_default_content()
                    sleep(2)
            return ads
        finally:
            try:
                firefox_driver.close()
            except:
                self._logger.debug('Failed closing close firefox driver')
