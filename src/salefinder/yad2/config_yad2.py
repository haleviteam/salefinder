﻿# coding=utf-8


import os

MAX_FAULTS = 5

if os.name == 'nt':
    FIREFOX_PROFILE_PATH = r'C:/Users/GUY/AppData/Roaming/Mozilla/Firefox/Profiles/lqy9k6hl.default'
else:
    if os.environ['USER'].lower() == 'guy':
        FIREFOX_PROFILE_PATH = r'/home/guy/.mozilla/firefox/5bk7aalq.default'
    else:
        FIREFOX_PROFILE_PATH = r'/root/.mozilla/firefox/okojzlij.default/'

GOOD_LOCATIONS = [
    u'הוד השרון',
    u'פתח תקוה',
    u'פתח תקווה',
    u'תל אביב',
    u'כפר סבא',
    u'נתניה',
    u'ראש העין',
    u'רמת השרון',
    u'דרום השרון',
    u'צפון השרון',
    u'רעננה',
    u'רמת גן',
    u'בני ברק',
]

ALL_LOCATIONS = [
    u'חיפה והסביבה',
    u'קריות והסביבה',
    u'עכו - נהריה והסביבה',
    u'גליל עליון',
    u'הכנרת והסביבה',
    u'כרמיאל והסביבה',
    u'נצרת - שפרעם והסביבה',
    u'ראש פינה החולה',
    u'גליל תחתון',
    u'הגולן',
    u'חדרה זכרון ועמקים',
    u'זכרון וחוף הכרמל',
    u'חדרה והסביבה',
    u'קיסריה והסביבה',
    u'יקנעם טבעון והסביבה',
    u'עמק בית שאן',
    u'עפולה והעמקים',
    u'רמת מנשה',
    u'השרון',
    u'נתניה והסביבה',
    u'רמת השרון - הרצליה',
    u'רעננה - כפר סבא',
    u'הוד השרון והסביבה',
    u'דרום השרון',
    u'צפון השרון',
    u'מרכז',
    u'תל אביב',
    u'ראשון לציון והסביבה',
    u'חולון - בת ים',
    u'רמת גן - גבעתיים',
    u'פתח תקווה והסביבה',
    u'ראש העין והסביבה',
    u'בקעת אונו',
    u'רמלה - לוד',
    u'בני ברק - גבעת שמואל',
    u'עמק איילון',
    u'שוהם והסביבה',
    u'מודיעין והסביבה',
    u'אזור ירושלים',
    u'ירושלים',
    u'בית שמש והסביבה',
    u'הרי יהודה - מבשרת והסביבה',
    u'מעלה אדומים והסביבה',
    u'יהודה שומרון ובקעת הירדן',
    u'ישובי דרום ההר',
    u'ישובי שומרון',
    u'גוש עציון',
    u'בקעת הירדן וצפון ים המלח',
    u'אריאל וישובי יהודה',
    u'שפלה מישור חוף דרומי',
    u'נס ציונה - רחובות',
    u'אשדוד - אשקלון והסביבה',
    u'גדרה - יבנה והסביבה',
    u'קרית גת והסביבה',
    u'שפלה',
    u'דרום',
    u'באר שבע והסביבה',
    u'אילת וערבה',
    u'ישובי הנגב',
    u'הנגב המערבי',
    u'דרום ים המלח',
]
