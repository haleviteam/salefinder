# coding=utf-8

from salefinder.post import Post


class Yad2Post(Post):
    DB_PRICE = 'price'
    DB_LOCATION = 'location'

    def __init__(self, post_id, message, price, locations, link, date):
        super(Yad2Post, self).__init__(post_id, message)
        self.price = price
        self.locations = locations
        self.link = link
        self.date = date

    def __repr__(self):
        return (u"מודעה מיד 2:\n\nמחיר: %s\nתאריך:\n%s\nלינק:\n%s\nמיקומים: %s\nתוכן המודעה:\n\n%s\n\n" % (
            self.price, self.date, self.link, ', '.join(self.locations), self.message)).encode('utf-8')

    def is_in_good_location(self, locations_dict):
        if not self.locations:
            return True
        for location in self.locations:
            if location in [location for location in locations_dict if locations_dict[location]]:
                return True
        return False

    def to_mongo(self):
        mongo_dict = super(Yad2Post, self).to_mongo()
        mongo_dict[self.DB_PRICE] = self.price
        mongo_dict[self.DB_LOCATION] = self.locations
        mongo_dict[self.DB_LINK] = self.link
        mongo_dict[self.DB_LAST_UPDATED] = self.date
        return mongo_dict

    def to_mongo_according_to_db(self, object_from_db):
        self.relevant_by_admin = object_from_db[self.DB_RELEVANT_BY_ADMIN]
        if self.message != object_from_db[self.DB_MESSAGE]:
            self.relevant_by_admin = True
        if self.price != object_from_db[self.DB_PRICE]:
            self.relevant_by_admin = True
        return self.to_mongo()
